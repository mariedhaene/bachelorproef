# Bachelorproef
## TODO
### Wat er eerst moet gebeuren
- [ ] Inleiding (in het Engels?), dankwoord, motivatie in verslag zetten
- [ ] Sectie over voorbeeld in verslag afwerken
- [ ] C-operator uitwerken theoretisch

### Wat later nog kan
- [ ] Toepassingen vinden van niet-Hermitische Hamiltonians
- [ ] Voorbeeld bespreken (niet per se helemaal uitwerken) voor epsilon < 0
- [ ] Uitwerking over oplossingen "samenplakken" met bv. Airy functions
- [ ] Periode uitrekenen

## Doelen voor de nabijere toekomst
- [x] WKB theorie uitwerken en verstaan
- [x] Stuk over reele eigenwaarden in verslag typen
- [x] Formule met (n+1/2)pi proberen te achterhalen (ook WKB)
- [ ] EVENTUEEL: Deel over klassieke beweging verwerken

## Tegen woensdag 27/11
- [x] Mail sturen naar Tim/Martin over integraal die niet werkt
- [x] Eigenwaarden bepalen! Voor eps > 0 weliswaar (voor eps < 0 moeten we iets met Airy functies doen).

## Tegen woensdag 20/11
- [x] *Analytic continuation of eigenvalue problems* doornemen
- [x] Opzoeken over die ambetante niet-Hermitische Hamiltoniaan

## Tegen woensdag 13/11
- [x] Eigenwaardeprobleem in complexe vlak proberen te verstaan
- [x] Zeker eens *Introduction* helemaal lezen en vragen opschrijven
- [x] Misschien eens [dit filmpje](https://www.youtube.com/watch?v=Ljc4apxDaf8) bekijken
